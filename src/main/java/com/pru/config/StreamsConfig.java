package com.pru.config;

import com.pru.message.Channels;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableBinding(Channels.class)
public class StreamsConfig {


}