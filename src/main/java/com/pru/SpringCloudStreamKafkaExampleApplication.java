package com.pru;

import com.pru.message.Channels;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamMessageConverter;
import org.springframework.cloud.stream.schema.avro.AvroSchemaMessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.messaging.converter.MessageConverter;
import org.springframework.util.MimeType;

import java.io.IOException;

@EnableBinding(Channels.class)
@SpringBootApplication
public class SpringCloudStreamKafkaExampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringCloudStreamKafkaExampleApplication.class, args);
    }
    @Bean
    @StreamMessageConverter
    public MessageConverter userMessageConverter() throws IOException {
        AvroSchemaMessageConverter avroSchemaMessageConverter = new AvroSchemaMessageConverter(MimeType.valueOf("avro/bytes"));
        avroSchemaMessageConverter.setSchemaLocation(new ClassPathResource("avro-schema/test.avsc"));
        return avroSchemaMessageConverter;
    }
}
