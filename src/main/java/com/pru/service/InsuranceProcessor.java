package com.pru.service;

import com.pru.entity.GroupInsurance;
import org.springframework.stereotype.Service;

@Service
public interface InsuranceProcessor {


    GroupInsurance processIncomingRequests(GroupInsurance message);

    void publishOutgoingInsuranceRequests(String message);

    void publishOutgoingInsuranceRequests(GroupInsurance message);

}
