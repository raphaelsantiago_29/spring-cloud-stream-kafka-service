package com.pru.service;

import com.pru.controller.OutputController;
import com.pru.entity.GroupInsurance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class InsuranceProcessorImpl implements InsuranceProcessor {

    @Autowired
    OutputController outputController;

    @Override
    public GroupInsurance processIncomingRequests(GroupInsurance message) {

        UUID uuid = UUID.randomUUID();

        //message.concat("_Processed_Message+");


        return message;
    }

    @Override
    public void publishOutgoingInsuranceRequests(String message) {


    }

    @Override
    public void publishOutgoingInsuranceRequests(GroupInsurance message) {

        outputController.publishInsuranceRequests(message);
    }
}
