package com.pru.message;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;

public interface Channels {

    String TRADE_CONSUMER_CHANNEL = "trade_consumer_channel";
    String TRADE_PRODUCER_CHANNEL = "trade_producer_channel";

    @Output(Channels.TRADE_PRODUCER_CHANNEL)
    MessageChannel publishTrades();

    @Input(Channels.TRADE_CONSUMER_CHANNEL)
    SubscribableChannel consumeTrades();

    String INSURANCE_ACTION_CHANNEL = "insurance_action_channel";

    String VAS_ACTION_CHANNEL = "vas_action_channel";

    String VAS_NOTIFICATION_CHANNEL = "vas_notification_channel";

    String INSURANCE_NOTIFICATION_CHANNEL = "insurance_notification_channel";

    @Output(Channels.INSURANCE_ACTION_CHANNEL)
    MessageChannel publishInsuranceRequest();

    @Output(Channels.VAS_ACTION_CHANNEL)
    MessageChannel publishWasRequest();

    @Input(Channels.INSURANCE_NOTIFICATION_CHANNEL)
    SubscribableChannel consumeInsuranceResponse();

    @Input(Channels.VAS_NOTIFICATION_CHANNEL)
    SubscribableChannel consumeWasResponse();

}
