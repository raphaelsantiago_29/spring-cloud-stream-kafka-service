package com.pru.entity;

import java.util.UUID;

public class GroupInsurance {

	private String contents;
	private long time;
	private UUID uuid;
	private String state;

	public GroupInsurance() {

	}

	public GroupInsurance(String contents, long time) {

		this.contents = contents;
		this.time = time;
	}

	public String getContents() {

		return contents;
	}

	public long getTime() {

		return time;
	}

	public void setTime(long time) {

		this.time = time;
	}

	public UUID getUuid() {
		return uuid;
	}

	public void setUuid(UUID uuid) {
		this.uuid = uuid;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public GroupInsurance(String contents, long time, UUID uuid) {
		this.contents = contents;
		this.time = time;
		this.uuid = uuid;
	}
}
