package com.pru.entity;

import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public enum AcknowledgementType {

    DO_NOT_PROCESS("Do not process"), // Integration - Integrated, Integration - Cancelled
    NO_ACK("Never Acknowledged"),
    IN_PROGRESS_TAL("In progress TAL"),
    IN_PROGRESS_TRADEFLOW("In progress TradeFlow"),
    MATCHING_UNMATCHED("Matching - Unmatched"),
    MATCHING_MATCHED("Matching - Matched");

    private final String text;

    private static final Map<String, AcknowledgementType> ENUM_MAP;

    /**
     * @param text
     */
    AcknowledgementType(final String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    /* (non-Javadoc)
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
        return text;
    }

    // https://stackoverflow.com/a/37841094
    static {
        Map<String, AcknowledgementType> map = new ConcurrentHashMap<>();
        for (AcknowledgementType instance : AcknowledgementType.values()) {
            map.put(instance.getText(), instance);
        }
        ENUM_MAP = Collections.unmodifiableMap(map);
    }

    public static AcknowledgementType get(final String name) {
        AcknowledgementType type = ENUM_MAP.get(name);
        if (type != null) {
            return type;
        }

        return DO_NOT_PROCESS;
    }
}
