package com.pru.entity;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.OffsetDateTime;

public class TradeEvent {

    // Required key for NewRelic Event API
    private String eventType;

    // BRS Trade nugget properties
    private String tradeId;
    private String archive;
    private Integer archiveSequence;
    private LocalDate archiveDate;
    private LocalTime archiveTime;
    private OffsetDateTime received;
    private Long touchCount;
    private Long fund;
    private String invnum;
    private String instrument;
    private String ticker;
    private String counterparty;
    private String currency;
    private String securityGroup;
    private String securityType;
    private String txnType;
    private String portfolioName;
    private String principal;
    private BigDecimal principalSgd;
    private String originalFace;
    private String trader;
    private LocalDate settlementDate;

    // BNP F46 properties
    private String ackFilename;
    private Integer ackSequence;
    private LocalDate ackDate;
    private LocalTime ackTime;
    private String ackCode = AcknowledgementType.NO_ACK.name();
    private String ackMessage = AcknowledgementType.NO_ACK.getText();
    private Integer ackStatus = AcknowledgementType.NO_ACK.ordinal();

    public String getEventType() {
        return eventType;
    }

    public void setEventType(final String eventType) {
        this.eventType = eventType;
    }

    public String getTradeId() {
        return tradeId;
    }

    public void setTradeId(final String tradeId) {
        this.tradeId = tradeId;
    }

    public String getArchive() {
        return archive;
    }

    public void setArchive(final String archive) {
        this.archive = archive;
    }

    public Integer getArchiveSequence() {
        return archiveSequence;
    }

    public void setArchiveSequence(final Integer archiveSequence) {
        this.archiveSequence = archiveSequence;
    }

    public LocalDate getArchiveDate() {
        return archiveDate;
    }

    public void setArchiveDate(final LocalDate archiveDate) {
        this.archiveDate = archiveDate;
    }

    public LocalTime getArchiveTime() {
        return archiveTime;
    }

    public void setArchiveTime(final LocalTime archiveTime) {
        this.archiveTime = archiveTime;
    }

    public OffsetDateTime getReceived() {
        return received;
    }

    public void setReceived(final OffsetDateTime received) {
        this.received = received;
    }

    public Long getTouchCount() {
        return touchCount;
    }

    public void setTouchCount(final Long touchCount) {
        this.touchCount = touchCount;
    }

    public Long getFund() {
        return fund;
    }

    public void setFund(final Long fund) {
        this.fund = fund;
    }

    public String getInvnum() {
        return invnum;
    }

    public void setInvnum(final String invnum) {
        this.invnum = invnum;
    }

    public String getInstrument() {
        return instrument;
    }

    public void setInstrument(final String instrument) {
        this.instrument = instrument;
    }

    public String getTicker() {
        return ticker;
    }

    public void setTicker(final String ticker) {
        this.ticker = ticker;
    }

    public String getCounterparty() {
        return counterparty;
    }

    public void setCounterparty(final String counterparty) {
        this.counterparty = counterparty;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(final String currency) {
        this.currency = currency;
    }

    public String getSecurityGroup() {
        return securityGroup;
    }

    public void setSecurityGroup(final String securityGroup) {
        this.securityGroup = securityGroup;
    }

    public String getSecurityType() {
        return securityType;
    }

    public void setSecurityType(final String securityType) {
        this.securityType = securityType;
    }

    public String getTxnType() {
        return txnType;
    }

    public void setTxnType(final String txnType) {
        this.txnType = txnType;
    }

    public String getPortfolioName() {
        return portfolioName;
    }

    public void setPortfolioName(final String portfolioName) {
        this.portfolioName = portfolioName;
    }

    public String getPrincipal() {
        return principal;
    }

    public void setPrincipal(final String principal) {
        this.principal = principal;
    }

    public BigDecimal getPrincipalSgd() {
        return principalSgd;
    }

    public void setPrincipalSgd(final BigDecimal principalSgd) {
        this.principalSgd = principalSgd;
    }

    public String getOriginalFace() {
        return originalFace;
    }

    public void setOriginalFace(final String originalFace) {
        this.originalFace = originalFace;
    }

    public String getTrader() {
        return trader;
    }

    public void setTrader(final String trader) {
        this.trader = trader;
    }

    public LocalDate getSettlementDate() {
        return settlementDate;
    }

    public void setSettlementDate(final LocalDate settlementDate) {
        this.settlementDate = settlementDate;
    }

    public String getAckFilename() {
        return ackFilename;
    }

    public void setAckFilename(final String ackFilename) {
        this.ackFilename = ackFilename;
    }

    public Integer getAckSequence() {
        return ackSequence;
    }

    public void setAckSequence(final Integer ackSequence) {
        this.ackSequence = ackSequence;
    }

    public LocalDate getAckDate() {
        return ackDate;
    }

    public void setAckDate(final LocalDate ackDate) {
        this.ackDate = ackDate;
    }

    public LocalTime getAckTime() {
        return ackTime;
    }

    public void setAckTime(final LocalTime ackTime) {
        this.ackTime = ackTime;
    }

    public String getAckCode() {
        return ackCode;
    }

    public void setAckCode(final String ackCode) {
        this.ackCode = ackCode;
    }

    public String getAckMessage() {
        return ackMessage;
    }

    public void setAckMessage(final String ackMessage) {
        this.ackMessage = ackMessage;
    }

    public Integer getAckStatus() {
        return ackStatus;
    }

    public void setAckStatus(final Integer ackStatus) {
        this.ackStatus = ackStatus;
    }
}
