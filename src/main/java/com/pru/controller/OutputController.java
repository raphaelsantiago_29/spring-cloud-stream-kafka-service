package com.pru.controller;

import com.pru.entity.GroupInsurance;
import com.pru.message.Channels;
import com.pru.service.InsuranceProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;
import org.springframework.util.MimeTypeUtils;

@Component
public class OutputController {

    @Autowired
    InsuranceProcessor insuranceProcessor;

    private final Channels channels;

    public OutputController(Channels channels) {
        this.channels = channels;
    }

    public void publishInsuranceRequests(GroupInsurance message) {

        MessageChannel messageChannel = channels.publishInsuranceRequest();
        messageChannel.send(MessageBuilder
                .withPayload(message)
                .setHeader(MessageHeaders.CONTENT_TYPE, MimeTypeUtils.APPLICATION_JSON)
                .build());
    }
}
