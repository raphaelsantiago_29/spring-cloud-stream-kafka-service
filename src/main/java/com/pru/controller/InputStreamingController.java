package com.pru.controller;

import com.pru.dto.TradeEventDTO;
import com.pru.message.Channels;
import com.pru.service.InsuranceProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.stereotype.Component;


@Component
public class InputStreamingController {

    private static final Logger logger = LoggerFactory.getLogger(InputStreamingController.class);

    @Autowired
    InsuranceProcessor insuranceProcessor;

    private final Channels channels;

    public InputStreamingController(Channels channels) {
        this.channels = channels;
    }

    //
    @StreamListener(Channels.TRADE_CONSUMER_CHANNEL)
    public void consume(String message) {

        logger.info("recieved a string message : " + message);
    }

    @StreamListener(Channels.TRADE_PRODUCER_CHANNEL)
    public void consumeFromProducer(TradeEventDTO message) {

        logger.info("recieved a string message on: " +Channels.TRADE_PRODUCER_CHANNEL + " ____ " +message);
    }
}
