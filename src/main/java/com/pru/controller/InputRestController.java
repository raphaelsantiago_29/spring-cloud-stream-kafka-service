package com.pru.controller;

import com.pru.dto.TestDTO;
import com.pru.dto.TradeEventDTO;
import com.pru.entity.GroupInsurance;
import com.pru.message.Channels;
import com.pru.service.InsuranceProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.util.MimeType;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;


@RestController
public class InputRestController {

    private static final Logger logger = LoggerFactory.getLogger(InputRestController.class);

    @Autowired
    InsuranceProcessor insuranceProcessor;

    private final Channels channels;

    public InputRestController(Channels channels) {
        this.channels = channels;
    }

//    @RequestMapping(value = "/pushTrades", method = RequestMethod.POST)
//    public void processInputRestMessages(@RequestBody String payload) {
//
//        String processedMessage = insuranceProcessor.processIncomingRequests(payload);
//        insuranceProcessor.publishOutgoingInsuranceRequests(processedMessage);
//    }

    @RequestMapping(value = "/purchase", method = RequestMethod.POST)
    public void processInputInsuranceRequest(@RequestParam String type, @RequestParam UUID uuid, @RequestParam String state) {

        logger.info("Request received from UI for Group Insurance value {} with uuid {}  and state {} ",type,uuid,state);
        GroupInsurance gp = new GroupInsurance();
        gp.setUuid(uuid);
        gp.setState(state);
        GroupInsurance processedMessage = insuranceProcessor.processIncomingRequests(gp);
        insuranceProcessor.publishOutgoingInsuranceRequests(processedMessage);
    }

//    @RequestMapping(value = "/callable", method = RequestMethod.GET)
//    public Callable<ResponseEntity<?>> timeCallable() {
//        log.info("Callable time request");
//        return () -> ResponseEntity.ok(now());
//    }

    @RequestMapping(value = "/pushTrades/avro/", method = RequestMethod.POST)
    public void publishMessageString(@RequestBody String payload) {
        TradeEventDTO tradeEventDTO = createSampleData();
        TestDTO testDTO = createTestData();
        // send message to channel
        MessageChannel messageChannel = channels.publishTrades();
        Message<TestDTO> testEventDTOMessage = MessageBuilder.withPayload(testDTO)
               // .setHeader("type","application/+avro").build();
        .setHeader(MessageHeaders.CONTENT_TYPE, MimeType.valueOf("application/*+avro")).build();
       // .setHeader(MessageHeaders.CONTENT_TYPE, "application/json").build();

        messageChannel.send(testEventDTOMessage);
    }

    private TradeEventDTO createSampleData(){
        TradeEventDTO tradeEventDTO =  new TradeEventDTO();
        tradeEventDTO.setReceived(1);
        tradeEventDTO.setFund(12345);
        tradeEventDTO.setArchiveSequence(9999);
        return tradeEventDTO;
    }

    private TestDTO createTestData(){
        TestDTO testDto = new TestDTO();
        testDto.setArchive("adsdf");
        testDto.setEventType("EventType");
        testDto.setTradeId("1234");
        return testDto;
    }
}
