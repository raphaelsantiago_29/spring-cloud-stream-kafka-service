package com.pru.controller;

import com.pru.entity.GroupInsurance;
import com.pru.message.Channels;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Sink;

@EnableBinding({Sink.class, Channels.class})
public class Consumer {

	private static final Logger logger = LoggerFactory.getLogger(Consumer.class);

	@StreamListener(target = Channels.INSURANCE_NOTIFICATION_CHANNEL)
	public void consumeInsuranceRequest(GroupInsurance groupInsurance) {

		logger.info("Response received Async for uuid {} state {} resonse {} ",groupInsurance.getUuid(),groupInsurance.getState(),groupInsurance.getContents());

	}


}
